# targetd-client-go

`targetd-client-go` is a client library for the targetd API written in Go.

## Contributing

This library is [Free Software](LICENSE) and every contributions are welcome.

Please note that this project is released with a [Contributor Code of
Conduct](CODE_OF_CONDUCT.md). By participating in this project you agree to
abide by its terms.
